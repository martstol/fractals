#include "rendering.hpp"
#include "util.hpp"
#include <GL/glew.h>
#include <GL/glfw.h>
#include <iostream>
#include <vector>
#include <stdexcept>

static GLuint vao;
static GLint widthUnifLocation;
static GLint heightUnifLocation;
static GLint timeUnifLocation;

GLuint loadShaderFromFile(std::string shaderPath, GLenum shaderType) {
	std::string source = read_file(shaderPath);
	const char * src = source.c_str();

	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &src, nullptr);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetShaderInfoLog(shader, 512, nullptr, buffer);
		throw std::runtime_error{buffer};
	}

	return shader;
}

void createShader(std::string vertexShaderPath, std::string fragmentShaderPath) {
	GLuint vertexShader = loadShaderFromFile(vertexShaderPath, GL_VERTEX_SHADER);
	GLuint fragmentShader = loadShaderFromFile(fragmentShaderPath, GL_FRAGMENT_SHADER);

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	GLint status;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		glGetProgramInfoLog(shaderProgram, 512, nullptr, buffer);
		throw std::runtime_error{buffer};
	}

	GLint posAttrib = glGetAttribLocation(shaderProgram, "pos");
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(posAttrib);

	widthUnifLocation = glGetUniformLocation(shaderProgram, "width");
	heightUnifLocation = glGetUniformLocation(shaderProgram, "height");
	timeUnifLocation = glGetUniformLocation(shaderProgram, "time");

	glUseProgram(shaderProgram);
}

void createTexture(std::string texturePath) {
	GLFWimage img;
	GLint status = glfwReadImage(texturePath.c_str(), &img, 0);
	if (status != GL_TRUE) {
		std::string msg = "Could not find " + texturePath;
		throw std::runtime_error{msg};
	}

	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_1D, tex);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, img.Width, 0, GL_RGB, GL_UNSIGNED_BYTE, img.Data);
	glGenerateMipmap(GL_TEXTURE_1D);

	glfwFreeImage(&img);
}

void setupOpenGL(std::string vertexShaderPath, std::string fragmentShaderPath, std::string texturePath) {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<float> vertices = {
		-1, -1, // v1
		 3, -1, // v2
		-1,  3,	// v3
	};
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(float), vertices.data(), GL_STATIC_DRAW);

	createShader(vertexShaderPath, fragmentShaderPath);
	createTexture(texturePath);
}

void render(int width, int height, float time) {
	glUniform1f(widthUnifLocation, float(width));
	glUniform1f(heightUnifLocation, float(height));
	glUniform1f(timeUnifLocation, time);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

