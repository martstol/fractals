#include "rendering.hpp"
#include <GL/glew.h>
#include <GL/glfw.h>
#include <iostream>
#include <thread>

using std::cout;
using std::endl;

static bool running = false;
static int width = 800;
static int height =600;

int GLFWCALL closeWindowCallback() {
	running = false;
	return GL_TRUE;
}

void GLFWCALL resizeWindowCallback(int w, int h) {
	width = w;
	height = h;
	glViewport(0, 0, w, h);
}

void printGLInfo() {
	cout << "OpenGL Version:  " << glGetString(GL_VERSION) << endl;
	cout << "OpenGL Vendor:   " << glGetString(GL_VENDOR) << endl;
	cout << "OpenGL Renderer: " << glGetString(GL_RENDERER) << endl;
	cout << "GLSL Version:    " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}

int main(int argc, char * argv[]) {
	if (argc < 4) {
		cout << "Usage: " << argv[0] << " vertex_shader fragment_shader 1d_texture" << endl;
		return -1;
	}

	if (!glfwInit()) {
		cout << "Init glfw failed" << endl;
		return -1;
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);

	if (glfwOpenWindow(width, height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW) == GL_FALSE) {
		cout << "Failed to open window" << endl;
		glfwTerminate();
		return -1;
	}

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		cout << "Failed to initialize glew" << endl;
		glfwTerminate();
		return -1;
	}

	glfwSetWindowCloseCallback(closeWindowCallback);
	glfwSetWindowSizeCallback(resizeWindowCallback);
	glfwSetWindowTitle("Fractals");

	printGLInfo();

	setupOpenGL(argv[1], argv[2], argv[3]);

	running = true;
	while (running) {
		render(width, height, float(glfwGetTime()));
		glfwSwapBuffers();
		glfwPollEvents();
		std::this_thread::yield();
	}

	glfwCloseWindow();
	glfwTerminate();
}
