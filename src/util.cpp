#include "util.hpp"

#include <fstream>
#include <sstream>
#include <stdexcept>

std::string read_file(std::string path) {
	std::ifstream in{path};
	if (in.is_open()) {
		std::ostringstream ss{};
		ss << in.rdbuf();
		return ss.str();
	}
	else {
		std::string msg = "Could not find " + path;
		throw std::runtime_error{msg};
	}
}

