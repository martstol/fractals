all: src/main.cpp src/rendering.cpp src/util.cpp
	clang++ -std=c++11 -O2 -Weverything src/main.cpp src/rendering.cpp src/util.cpp -o main -Iinclude -lglfw -lGLEW -lGL -Wno-c++98-compat -Wno-unused-parameter -Wno-missing-prototypes
