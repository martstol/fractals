#pragma once

#include <string>

void setupOpenGL(std::string vertexShaderPath, std::string fragmentShaderPath, std::string texturePath);
void render(int width, int height, float time);

