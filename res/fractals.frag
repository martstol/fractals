#version 130

in vec2 uv;

out vec4 outColor;

uniform float time;
uniform float width;
uniform float height;
uniform sampler1D tex;

vec2 complexMult(vec2 a, vec2 b) {
	float r = a.x*b.x - a.y*b.y;
	float i = a.x*b.y + a.y*b.x;
	return vec2(r, i);
}

float mandelbrot(vec2 c) {
	float maxItr = 100.0;
	vec2 z = vec2(0.0);
	for (float i = 0.0; i < maxItr; i++) {
		if (z.x*z.x + z.y*z.y > 4) {
			return (i + 1 - log(log(sqrt(z.x*z.x+z.y*z.y))) / log(2)) / maxItr;
		}
		
		z = complexMult(z, z) + c;
	}
	return 1.0;
}

void main() {
	vec2 ratio = vec2(width/height, 1);
	vec2 coords = ratio*uv;
	
	coords.x = 2.5*coords.x - 2.4;
	coords.y = 2.5*coords.y - 1.25;

	float i = mandelbrot(coords);
	outColor = texture(tex, i);
}
