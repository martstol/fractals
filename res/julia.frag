#version 130

in vec2 uv;

out vec4 outColor;

uniform float time;
uniform float width;
uniform float height;
uniform sampler1D tex;

vec2 complex_square(vec2 z) {
	return vec2(z.x*z.x - z.y*z.y, 2.0*z.x*z.y);
}

void main() {
	vec2 ratio = vec2(width/height, 1);
	vec2 coords = 2.5*ratio*(uv - 0.5);
	vec2 c = vec2(cos(0.75*time), sin(0.75*time));
	vec2 z = vec2(coords);
	int iter = 100;

	int i;
	for(i=0; i<iter; i++) {
		vec2 val = complex_square(z) + c;
		if(val.x*val.x + val.y*val.y > 4.0) break;
		z = val;
	}

	gl_FragColor = texture1D(tex, (i == iter ? 1.0 : float(i)/100.0));
}
